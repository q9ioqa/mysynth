/*
  ==============================================================================

    OscData.h
    Created: 1 Nov 2021 11:16:10am
    Author:  Bence

  ==============================================================================
*/

#include <JuceHeader.h>
#pragma once

class OscData : public juce::dsp::Oscillator<float>
{
public:
    void prepareToPlay(juce::dsp::ProcessSpec& spec);
    void setType(const int oscSelection);
    void setGain(const float levelInDecibels);
    void setPitchVal(const int pitch);
    void setFreq(const int midiNoteNumber);
    void getNextAudioBlock(juce::dsp::AudioBlock<float>& audioBlock);
    void updateFm(const float freq, const float depth);

private:
    juce::dsp::Gain<float> gain;
    int pitchVal{ 0 };
    int lastMidiNote{ 0 };

    void processFmOsc(juce::dsp::AudioBlock<float>& block);

    juce::dsp::Oscillator<float> fmOsc{ [](float x) { return std::sin(x); } };
    float fmMod{ 0.0f };
    float fmDepth{ 0.0f };
};