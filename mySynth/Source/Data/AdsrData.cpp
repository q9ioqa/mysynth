/*
  ==============================================================================

    AdsrData.cpp
    Created: 31 Oct 2021 6:07:06pm
    Author:  Bence

  ==============================================================================
*/

#include "AdsrData.h"
void AdsrData::update(const float attack, const float decay, const float sustain, const float release)
{
    adsrParams.attack = attack;
    adsrParams.decay = decay;
    adsrParams.sustain = sustain;
    adsrParams.release = release;

    setParameters(adsrParams);
}