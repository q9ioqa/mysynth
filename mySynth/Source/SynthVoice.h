/*
  ==============================================================================

    SynthVoice.h
    Created: 30 Oct 2021 3:51:41pm
    Author:  Bence

  ==============================================================================
*/

#include <JuceHeader.h>
#include "SynthSound.h"
#include "Data/AdsrData.h"
#include "Data/OscData.h"
#pragma once

class SynthVoice : public juce::SynthesiserVoice
{
public:
    bool canPlaySound(juce::SynthesiserSound* sound) override;
    void startNote(int midiNoteNumber, float velocity, juce::SynthesiserSound* sound, int currentPitchWheelPosition) override;
    void stopNote(float velocity, bool allowTailOff) override;
    void controllerMoved(int controllerNumber, int newConrollerValue) override;
    void renderNextBlock(juce::AudioBuffer<float>& outputBuffer, int startSample, int numSamples) override;
    void pitchWheelMoved(int newPitchWheelValue) override;
    void prepareToPlay(double sampleRate, int samplesPerBlock, int outputChannels);

    void updateADSR(const float attack, const float decay, const float sustain, const float release);

    OscData& getOscillator1() { return osc1; }
    OscData& getOscillator2() { return osc2; }
    AdsrData& getAdsr() { return adsr; }

private:
    juce::AudioBuffer<float> SynthBuffer;

    AdsrData adsr;
    OscData osc1;
    OscData osc2;;
    juce::dsp::Gain<float> gain;

    bool isPrepared{ false };    
};