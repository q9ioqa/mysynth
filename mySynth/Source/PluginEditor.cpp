/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "UI/OscComponent.h"
#include "DATA/OscData.h"

//==============================================================================
MySynthAudioProcessorEditor::MySynthAudioProcessorEditor (MySynthAudioProcessor& p)
    : AudioProcessorEditor(&p), audioProcessor(p), osc1(audioProcessor.apvts, "OSC1TYPE", "OSC1FMFREQ", "OSC1FMDEPTH"), adsr(audioProcessor.apvts)
{
    setSize(400, 300);
    addAndMakeVisible(osc1);
    addAndMakeVisible(adsr);
}

MySynthAudioProcessorEditor::~MySynthAudioProcessorEditor()
{
}

//==============================================================================
void MySynthAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void MySynthAudioProcessorEditor::resized()
{
    adsr.setBounds(getWidth() / 2, 0, getWidth() / 2, getHeight());
    osc1.setBounds(0, 25, getWidth() / 2, 100);
    osc2.setBounds(0, 150, getHeight() / 2, 100);
}
