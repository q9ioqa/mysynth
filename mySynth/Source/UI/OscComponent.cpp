/*
  ==============================================================================

    OscComponent.cpp
    Created: 1 Nov 2021 11:50:37am
    Author:  Bence

  ==============================================================================
*/

#include <JuceHeader.h>
#include "OscComponent.h"

//==============================================================================
OscComponent::OscComponent(juce::AudioProcessorValueTreeState& apvts, juce::String oscId, juce::String fmFreqId, juce::String fmDepthId)
{
    juce::StringArray choices{ "Sine", "Saw", "Square" };
    oscSelector.addItemList(choices, 1);
    addAndMakeVisible(oscSelector);

    oscSelAttachment = std::make_unique<juce::AudioProcessorValueTreeState::ComboBoxAttachment>(apvts, oscId, oscSelector);

    setSliderWithLabel(fmFreqSlider, fmFreqLabel, apvts, fmFreqId, fmFreqAttachment);
    setSliderWithLabel(fmDepthSlider, fmDepthLabel, apvts, fmDepthId, fmDepthAttachment);

    //GAIN AND PITCH
    /*
gainSlider.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
gainSlider.setTextBoxStyle(juce::Slider::TextBoxBelow, true, 50, 25);
addAndMakeVisible(gainSlider);

pitchSlider.setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
pitchSlider.setTextBoxStyle(juce::Slider::TextBoxBelow, true, 50, 25);
addAndMakeVisible(pitchSlider);

gainAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(apvts, gainId, gainSlider);

pitchAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(apvts, pitchId, pitchSlider);
*/
}


OscComponent::~OscComponent()
{
}

void OscComponent::paint(juce::Graphics& g)
{
    g.fillAll(juce::Colours::black);    
    g.setColour(juce::Colours::white);
    g.drawRect(getLocalBounds());
}

void OscComponent::resized()
{
    const auto sliderPosY = 80;
    const auto sliderWidth = 100;
    const auto sliderHeight = 90;
    const auto labelYOffset = 20;
    const auto labelHeight = 20;
    
    oscSelector.setBounds (0, 0, 90, 20);
    fmFreqSlider.setBounds (0, sliderPosY, sliderWidth, sliderHeight);
    fmFreqLabel.setBounds (fmFreqSlider.getX(), fmFreqSlider.getY() - labelYOffset, fmFreqSlider.getWidth(), labelHeight);
    
    fmDepthSlider.setBounds (fmFreqSlider.getRight(), sliderPosY, sliderWidth, sliderHeight);
    fmDepthLabel.setBounds (fmDepthSlider.getX(), fmDepthSlider.getY() - labelYOffset, fmDepthSlider.getWidth(), labelHeight);
}
void OscComponent::setSliderWithLabel(juce::Slider& slider, juce::Label& label, juce::AudioProcessorValueTreeState& apvts, juce::String paramId, std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment>& attachment)
{
    slider.setSliderStyle(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    slider.setTextBoxStyle(juce::Slider::TextBoxBelow, true, 50, 25);
    addAndMakeVisible(slider);

    attachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(apvts, paramId, slider);

    label.setColour(juce::Label::ColourIds::textColourId, juce::Colours::white);
    label.setFont(15.0f);
    label.setJustificationType(juce::Justification::centred);
    addAndMakeVisible(label);
}