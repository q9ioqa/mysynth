/*
  ==============================================================================

    SynthSound.h
    Created: 30 Oct 2021 3:51:54pm
    Author:  Bence

  ==============================================================================
*/

#include <JuceHeader.h>
#pragma once

class SynthSound : public juce::SynthesiserSound
{
public:
    bool appliesToNote (int midiNoteNumber) override { return true; }
    bool appliesToChannel(int midiChannel) override { return true; }


};